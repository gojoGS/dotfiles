# Catalog

## Manjaro i3

### zsh

.zshrc **~/**

.p10k.zsh **~/**

### NeoVim

init.vim **~/.config/nvim/**

### Rofi

dmenu.sh **~/.config/rofi/**

run.sh **~/.config/rofi/**

window.sh **~/.config/rofi/**

### picom 

picom.conf **~/.config/**

### i3-gaps

config **~/.i3**

## Kubuntu


#!/bin/bash

gitacp () {
	git add .
	git commit -am $1
	git push
}

mcdir () {
	mkdir $1 && cd $1
}

ccompile () {
	gcc $1.c -o $1
}

cppcompile () {
	g++ $1.cpp -o $1
}

#####
# compileall
#
#	Compiles all cpp files in directory
#####
compileall() {
	for f in *.cpp
	do
		g++ $f -o $f.o
	done

}

#####
# shrek username
#
# Connects to shrek server with ssh (default port: 2222; might parameterize that)
#####
shrek () {
	ssh $1@shrek.unideb.hu -p 2222
}

#####
# shrekGet username remoteFileName gonnaBeFileName
#
# Grabs a remote file with scp
#####
shrekGet (){
	scp -P 2222 $1@shrek.unideb.hu:$2 $3
}

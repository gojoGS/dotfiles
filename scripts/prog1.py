import re
import sys
import os
import git
from termcolor import colored

with open(sys.argv[1], "r") as f_urls:
    urls = f_urls.read().splitlines()

link = r'.*?github.com/([^/]+)/([^/]+)/?.*'

for url in urls:
    search = re.search(link, url)

    if search:
        username = search.group(1)
        projectname = search.group(2)
        if any([username, projectname]) is not None:
            print("[{}] {}: {}".format( colored("   OK   ", "green"), colored("RegexMatch", "blue"), username))
            repo_url = "https://github.com/"+username+"/"+projectname+"/"
            if os.path.isdir("./"+username+"/"):
                print("[{}] {}: {}\n".format(colored(" Exists ", "yellow"), colored("Cloning", "blue"), username))
            else:
                try:
                    git.Repo.clone_from(repo_url, username)
                    print("[{}] {}: {}\n".format(colored("   OK   ", "green"), colored("Cloning", "blue"), username))
                except git.exc.GitError:
                    print("[{}] {}: {}\n".format(colored(" Failed ", "red"), colored("Cloning", "blue"), username))

    else:
        print("[{}] {}: {}\n".format( colored(" Failed ", "red"), colored("Invalid URL", "yellow"), url))
